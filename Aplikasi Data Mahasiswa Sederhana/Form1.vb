﻿Imports Microsoft.Office.Interop
Public Class Form1

    Dim mRow As Integer = 0
    Dim newpage As Boolean = True

    Private Sub btnPilhFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPilhFile.Click
        Dim xlApp As Excel.Application
        Dim xlWorkBook As Excel.Workbook
        Dim xlWorkSheet As Excel.Worksheet
        Dim xlRange As Excel.Range
        Dim xlRow As Integer
        Dim students As Object

        Dim strFilename As String
        Dim data(0 To 100) As String

        With OpenFileDialog1
            .Filter = "Excel Office| *.xls;*.xlsx"
            .ShowDialog()
            strFilename = .FileName
        End With

        If strFilename <> String.Empty Then
            DataGridView1.Columns(1).Width = 200
            xlApp = New Excel.Application
            xlWorkBook = xlApp.Workbooks.Open(strFilename)
            xlWorkSheet = xlWorkBook.Worksheets("Sheet1")
            xlRange = xlWorkSheet.UsedRange
            students = xlRange
            For xlRow = 2 To students.Rows.Count
                DataGridView1.Rows.Add(students.Cells(xlRow, 1).Text, students.Cells(xlRow, 2).Text, students.Cells(xlRow, 3).Text, NilaiHuruf(Convert.ToInt32(students.Cells(xlRow, 3).Text)))
            Next
            xlWorkBook.Close()
            xlApp.Quit()
            btnPrint.Enabled = True
        End If
    End Sub

    'function procedure, di panggil saat looping untuk menunjukan nilai dalam bentuk huruf
    Function NilaiHuruf(ByRef nilai As Integer) As String
        If nilai >= 90 Then
            Return "A"
        ElseIf nilai >= 80 Then
            Return "B"
        ElseIf nilai >= 70 Then
            Return "C"
        ElseIf nilai >= 60 Then
            Return "D"
        Else
            Return "F"
        End If
    End Function
    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        With DataGridView1
            Dim fmt As StringFormat = New StringFormat(StringFormatFlags.LineLimit)
            fmt.LineAlignment = StringAlignment.Center
            fmt.Trimming = StringTrimming.EllipsisCharacter
            Dim y As Single = e.MarginBounds.Top
            Do While mRow < .RowCount
                Dim row As DataGridViewRow = .Rows(mRow)
                Dim x As Single = e.MarginBounds.Left
                Dim h As Single = 0
                For Each cell As DataGridViewCell In row.Cells
                    Dim rc As RectangleF = New RectangleF(x, y, cell.Size.Width, cell.Size.Height)
                    e.Graphics.DrawRectangle(Pens.Black, rc.Left, rc.Top, rc.Width, rc.Height)
                    If (newpage) Then
                        e.Graphics.DrawString(DataGridView1.Columns(cell.ColumnIndex).HeaderText, .Font, Brushes.Black, rc, fmt)
                    Else
                        e.Graphics.DrawString(DataGridView1.Rows(cell.RowIndex).Cells(cell.ColumnIndex).FormattedValue.ToString(), .Font, Brushes.Black, rc, fmt)
                    End If
                    x += rc.Width
                    h = Math.Max(h, rc.Height)
                Next
                newpage = False
                y += h
                mRow += 1
                If y + h > e.MarginBounds.Bottom Then
                    e.HasMorePages = True
                    mRow -= 1
                    newpage = True
                    Exit Sub
                End If
            Loop
            mRow = 0
        End With
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        PrintDocument1.Print()
    End Sub

    Sub clearDataGrid()
        btnPrint.Enabled = False
        DataGridView1.Rows.Clear()
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        clearDataGrid()
    End Sub
End Class
